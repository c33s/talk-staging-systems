import React from 'react';
import './Link.css';

const Link = ({ children, href }) => (
  <a className="Link" target="_blank" href={href}>
    {children}
  </a>
);

export default Link;
